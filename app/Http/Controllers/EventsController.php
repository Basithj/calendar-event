<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;

class EventsController extends Controller
{
    public function getAllData(){
        $events = Events::all();

        return $events;

    }

    public function saveData(Request $request){
        
        $events = new Events;

        $events->title = $request->title;
        $events->start = $request->start;
        $events->end = $request->end;
        $events->daysOfWeek = $request->daysOfWeek;

        $events->save();

    }
}
